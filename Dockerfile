FROM docker.io/library/node:18

RUN useradd -ms /bin/bash nodeuser

USER nodeuser

ENV HOME=/home/nodeuser
ENV NPM_PACKAGES="${HOME}/.npm-packages"
ENV NODE_PATH=${NPM_PACKAGES}/lib/node_modules:$NODE_PATH
ENV PATH=${NPM_PACKAGES}/bin:$PATH

RUN mkdir "${HOME}/.npm-packages" && \
    echo "prefix=${HOME}/.npm-packages" >> ${HOME}/.npmrc

RUN npm install -g @sanity/cli@3

WORKDIR /home/nodeuser
